const Router = require('express').Router();

const {
    ServerError
} = require('./errors')

const {
    getMovies,
    getMovieById,
    addMovie,
    updateMovieById,
    deleteMovieById
} = require('./services.js');

Router.get('/', async(req, res) => {
    console.log("get all movies");
    const movies = await getMovies();
    res.json(movies);
});

Router.get('/:id', async(req, res) => {
    const {
        id
    } = req.params;

    const movie = await getMovieById(id);
    res.json(movie);
});

Router.post('/', async(req, res) => {
    const {
        title,
        genre,
        budget,
        release_date,
        imdb_score
    } = req.body;

    if (!title) {
        throw new ServerError('Title cannot be null', 400);
    }

    if (!genre) {
        throw new ServerError('Genre cannot be null', 400);
    }

    const id = await addMovie(title, genre, budget, release_date, imdb_score);
    res.json(id);
});

Router.put('/:id', async(req, res) => {
    const {
        id
    } = req.params;

    const {
        title,
        genre,
        budget,
        release_date,
        imdb_score
    } = req.body;

    const ret_id = await updateMovieById(id, title, genre, budget, release_date, imdb_score);
    res.json(ret_id);
});

Router.delete('/:id', async(req, res) => {
    const {
        id
    } = req.params;

    const movie = await deleteMovieById(id);
    res.json(movie);
});

module.exports = Router;
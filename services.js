const {
    sendRequest
} = require('./http-client');

const getMovies = async() => {
    console.info('Sending request to IO service to retrieve all movies');

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/movies/`
    }

    const movies = await sendRequest(options);
    return movies;
};

const getMovieById = async(id) => {
    console.info(`Sending request to IO service for movie with id ${id}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/movies/${id}`
    }

    const movie = await sendRequest(options);
    return movie;
};

const addMovie = async(title, genre, budget, release_date, imdb_score) => {
    console.info('Sending request to IO service to add new movie');

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/movies`,
        method: 'POST',
        data: {
            title,
            genre,
            budget,
            release_date,
            imdb_score
        }
    }

    const id = await sendRequest(options);
    return id;
};

const updateMovieById = async(id, title, genre, budget, release_date, imdb_score) => {
    console.info(`Sending request to IO service to update movie with id ${id}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/movies/${id}`,
        method: 'PUT',
        data: {
            title,
            genre,
            budget,
            release_date,
            imdb_score
        }
    }

    const ret_id = await sendRequest(options);
    return ret_id;
};

const deleteMovieById = async(id) => {
    console.info(`Sending request to IO service to delete movie with id ${id}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/movies/${id}`,
        method: 'DELETE'
    }

    const ret_id = await sendRequest(options);
    return ret_id;
};

module.exports = {
    getMovies,
    getMovieById,
    addMovie,
    updateMovieById,
    deleteMovieById
}